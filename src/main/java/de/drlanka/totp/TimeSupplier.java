package de.drlanka.totp;

import java.util.function.Supplier;

/**
 * Returns the current time in milliseconds since epoch.
 * Useful because, we simulate certain timestamps during testing.
 */
interface TimeSupplier extends Supplier<Long> {
}
