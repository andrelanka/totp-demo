package de.drlanka.totp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.crypto.spec.SecretKeySpec;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Authenticator {

  private static final Logger logger = LoggerFactory.getLogger(Authenticator.class);

  private final TimeBasedOneTimePasswordGenerator generator;
  private final MessageDigest messageDigest;
  private TimeSupplier timeSupplier = System::currentTimeMillis;

  Authenticator() {
    try {
      this.generator = new TimeBasedOneTimePasswordGenerator();
      this.messageDigest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e); //Constructors should not throw exceptions, but here it's ok, the algorithm is expected to be always there
    }
  }

  void setTimeSupplier(TimeSupplier timeSupplier) {
    this.timeSupplier = timeSupplier;
  }

  String getMatchingTimeCode(byte[] secretKey, String salt, String expectedHash) {
    try {
      long now = timeSupplier.get();
      long earlier = now - generator.getTimeStep(TimeUnit.MILLISECONDS);
      long later = now + generator.getTimeStep(TimeUnit.MILLISECONDS);
      long[] timestamps = {now, earlier, later};
      SecretKeySpec raw = new SecretKeySpec(secretKey, "RAW");
      for (long timestamp : timestamps) {
        int timeCode = generator.generateOneTimePassword(raw, new Date(timestamp));
        String formattedTimeCode = String.format("%06d", timeCode);
        String actualHash = hash(salt + formattedTimeCode);
        if (actualHash.equals(expectedHash)) {
          return formattedTimeCode;
        }
      }
    } catch (Exception e) {
      logger.error("Unable to validate hash " + expectedHash, e);
    }
    return null;
  }

  String generateUrl(String company, String application, byte[] secretKey) {
    try {
      String encodedCompany = URLEncoder.encode(company, "utf-8");
      String encodedApplication = URLEncoder.encode(application, "utf-8");
      String encodedSecret = new Base32().encodeToString(secretKey);
      return "otpauth://totp/"
          + encodedCompany + "%3A" + encodedApplication
          + "?secret=" + encodedSecret
          + "&issuer=" + encodedCompany
          + "&algorithm=SHA256"
          + "&digits=6"
          + "&period=30";
    } catch (UnsupportedEncodingException e) {
      logger.error("Unable to build URL for " + company + " with application " + application, e);
      throw new RuntimeException(e);
    }
  }

  String hash(String input) {
    messageDigest.reset();
    byte[] rawHash = messageDigest.digest(input.getBytes(StandardCharsets.UTF_8));
    return Hex.encodeHexString(rawHash);
  }
}