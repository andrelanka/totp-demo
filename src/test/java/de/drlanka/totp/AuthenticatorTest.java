package de.drlanka.totp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Calendar;
import java.util.Locale;

import org.apache.commons.codec.Charsets;
import org.junit.Test;

public class AuthenticatorTest {

  @Test
  public void checkHashCreation() {
    assertEquals(
        "46e6bf4c723ea261f239909f1a718aaca0efa5bdfbe9b5642900f3c7c42138b6",
        new Authenticator().hash("testPattern")
    );
  }

  @Test
  public void wrongHash() {
    Authenticator authenticator = new Authenticator();
    assertNull(authenticator.getMatchingTimeCode("x".getBytes(Charsets.UTF_8), "y", "z"));
  }

  @Test
  public void url() {
    Authenticator authenticator = new Authenticator();
    String expectedUrl = "otpauth://totp/ACME%3ANewApp?secret=NNSXS===&issuer=ACME&algorithm=SHA256&digits=6&period=30";
    assertEquals(expectedUrl, authenticator.generateUrl("ACME", "NewApp", "key".getBytes(Charsets.UTF_8)));
  }

  @Test
  public void validHash() {
    Authenticator authenticator = new Authenticator();
    Calendar calendar = Calendar.getInstance(Locale.GERMANY);
    authenticator.setTimeSupplier(calendar::getTimeInMillis);
    calendar.set(2018, Calendar.DECEMBER, 21, 15, 51, 35);
    String password = "s3cr3t";
    String expectedTimeCode = "599354";
    String expectedHash = authenticator.hash(password + expectedTimeCode);
    byte[] secretKey = "key".getBytes(Charsets.UTF_8);
    assertEquals(expectedTimeCode, authenticator.getMatchingTimeCode(secretKey, password, expectedHash));

    calendar.set(2018, Calendar.DECEMBER, 21, 15, 51, 5);
    assertEquals(expectedTimeCode, authenticator.getMatchingTimeCode(secretKey, password, expectedHash));
    calendar.set(2018, Calendar.DECEMBER, 21, 15, 52, 5);
    assertEquals(expectedTimeCode, authenticator.getMatchingTimeCode(secretKey, password, expectedHash));
    calendar.set(2018, Calendar.DECEMBER, 21, 15, 50, 35);
    assertNull(authenticator.getMatchingTimeCode(secretKey, password, expectedHash));
    calendar.set(2018, Calendar.DECEMBER, 21, 15, 52, 36);
    assertNull(authenticator.getMatchingTimeCode(secretKey, password, expectedHash));
  }

  @Test
  public void leadingZero() {
    Authenticator authenticator = new Authenticator();
    Calendar calendar = Calendar.getInstance(Locale.GERMANY);
    authenticator.setTimeSupplier(calendar::getTimeInMillis);
    calendar.set(2018, Calendar.DECEMBER, 21, 16, 12, 35);
    String password = "unknown";
    String expectedTimeCode = "057939";
    String expectedHash = authenticator.hash(password + expectedTimeCode);
    byte[] secretKey = "key".getBytes(Charsets.UTF_8);
    assertEquals(expectedTimeCode, authenticator.getMatchingTimeCode(secretKey, password, expectedHash));
  }
}